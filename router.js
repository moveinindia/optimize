import React from 'react';
import { Image } from 'react-native'
import { createStackNavigator } from 'react-navigation-stack'
import { createBottomTabNavigator } from 'react-navigation-tabs'
import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import { colors } from './Source/Assets/styles'
import Splash from './Source/Splash'


import Login from './Source/LoginSignup/Login'

import Home from './Source/Main/Home'

const HomeStack = createStackNavigator(
    {
        Home: Home,
    }, {
    headerMode: 'none'
}
);


export const AppTabs = createBottomTabNavigator(
    {
        Home: {
            screen: HomeStack,
            navigationOptions: {
                tabBarIcon: ({ tintColor }) => {
                    return (
                        <Image
                            source={require('./Source/Assets/Images/home.png')}
                            style={{ tintColor: tintColor,height:20,width:20,resizeMode:'contain' }}
                        />
                    )
                }
            }
        },
        EventCast: {
            screen: HomeStack,
            navigationOptions: {
                tabBarIcon: ({ tintColor }) => {
                    return (
                        <Image
                            source={require('./Source/Assets/Images/Group.png')}
                            style={{ tintColor: tintColor,height:20,width:20,resizeMode:'contain' }}
                        />
                    )
                }
            }
        },
        Explore: {
            screen: HomeStack,
            navigationOptions: {
                tabBarIcon: ({ tintColor }) => {
                    return (
                        <Image
                            source={require('./Source/Assets/Images/comment.png')}
                            style={{ tintColor: tintColor,height:20,width:20,resizeMode:'contain' }}
                        />
                    )
                }
            }
        },
        Network: {
            screen: HomeStack,
            navigationOptions: {
                tabBarIcon: ({ tintColor }) => {
                    return (
                        <Image
                            source={require('./Source/Assets/Images/envelope.png')}
                            style={{ tintColor: tintColor,height:20,width:20,resizeMode:'contain' }}
                        />
                    )
                }
            }
        },
    },
    
    {
        initialRouteName: 'Home',
        tabBarOptions: {
            activeTintColor: colors.gradHigh,
            inactiveTintColor: "gray",
            showIcon: true,
            tabBarPosition: 'bottom',
            labelStyle: {
                fontSize: 12,
            },
            iconStyle: {
                width: 30,
                height: 30
            },
            style: {
                backgroundColor: colors.white,
                borderBottomWidth: 1,
                borderBottomColor: '#ededed',
                alignItems: 'center',
                justifyContent: 'center',
                alignSelf: 'center',
            },
            lazy: true,
            indicatorStyle: '#fff',
        }
    })

const AuthStack = createStackNavigator(
    {
        Login: Login,
    }, {
    headerMode: 'none'
}
);


export default createAppContainer(createSwitchNavigator(
    {
        AuthLoading: Splash,
        App: AppTabs,
        Auth: AuthStack,
    },
    {
        initialRouteName: 'AuthLoading',
    }
));