import React, { Component } from 'react';
import { Text, View,SafeAreaView } from 'react-native';
import Routes from './router'
export default class App extends Component {
  render() {
    return (
      <SafeAreaView style={{flex:1}}>
      <Routes />
      </SafeAreaView>
    );
  }
}
