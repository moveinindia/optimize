import React from 'react';
import { StyleSheet } from 'react-native';
export const colors = {
    AppYellow: '#fadb39',
    black: '#000',
    white: '#fff',
    AppOrange: '#fa723e',
    AppGreen: '#54996f',
    tintGray: '#b3b3b3',
    gradHigh: "#763491",
    gradMid: "#893b96",
    gradLow: "#a44599"
}
export const CommonStyles = StyleSheet.create({
    card: {
        shadowColor: '#a9a9a9',
        shadowOffset: {
            width: 0,
            height: 1
        },
        shadowRadius: 5,
        shadowOpacity: 1.0,
        backgroundColor:'white',
        elevation:5
    }

})