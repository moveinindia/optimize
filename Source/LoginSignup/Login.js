import React, { Component } from 'react';
import {
    AsyncStorage,
    StatusBar,
    StyleSheet,
    Button,
    View,
    Dimensions,
    TouchableOpacity,
    Text
} from 'react-native';
import { colors,CommonStyles } from '../Assets/styles'
const { height, width }=Dimensions.get("window");
import { ButtonBordered } from '../components';
export default class SignInScreen extends Component {
    static navigationOptions = {
        title: 'Please sign in',
    };

    render() {
        return (
            <View style={styles.container}>
                <StatusBar barStyle="dark-content" backgroundColor={colors.gradHigh} />
                <Button title="Sign in!" onPress={this._signInAsync} />
<View style={[CommonStyles.card,{height:height*0.55,width:width*0.85,borderRadius:10}]} >
<ButtonBordered>
    <Text>ji ho</Text>
</ButtonBordered>
</View>
</View>
        );
    }

    _signInAsync = async () => {
        await AsyncStorage.setItem('Token', 'abc');
        this.props.navigation.navigate('App');
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
});