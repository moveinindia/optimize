import React from 'react';
import {
TouchableOpacity
} from 'react-native';
import { colors,CommonStyles } from '../Assets/styles';
export function ButtonBordered(props){
    return<TouchableOpacity style={{borderColor:colors.gradHigh,borderWidth:1,height:40,width:200,justifyContent:'center',alignItems:'center',borderRadius:20,backgroundColor:'white'}}>
   {props.children}
</TouchableOpacity>
}