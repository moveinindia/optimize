import React , { Component } from 'react';
import {
  AsyncStorage,
  Button,
  StatusBar,
  StyleSheet,
  View,
  ActivityIndicator
} from 'react-native';
import WebView from 'react-native-webview'
import {colors} from '../Assets/styles'

export default class HomeScreen extends Component {
    static navigationOptions = {
      title: 'Welcome to the app!',
    };
  
    render() {
      return (
        <View style={styles.container}>
         <StatusBar barStyle="dark-content" backgroundColor={"#fff"} />
         <View style={{flex:1,}}>
         <WebView 
         style={{flex:1}}
                incognito={true}
                source={{uri: 'https://www.facebook.com/'  }} 
                javaScriptEnabled = {true}
                startInLoadingState={false}
                startInLoadingState={true} 
                renderLoading={()=>{return<View style={{flex:1}}>
                  <ActivityIndicator size="large" color={colors.gradHigh} />
                </View>}}
                />
         </View>
          {/* <Button title="Actually, sign me out :)" onPress={this._signOutAsync} /> */}
        </View>
      );
    }
  
    _signOutAsync = async () => {
        await AsyncStorage.clear();
    //   await AsyncStorage.removeItem('Token');
      this.props.navigation.navigate('Auth');
    };
  }
  const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
  });