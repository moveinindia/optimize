
import React from 'react';
import {
    AsyncStorage,
    StatusBar,
    View,
    Image
} from 'react-native';
import { colors } from './Assets/styles';
import Video from 'react-native-video';
export default class AuthLoadingScreen extends React.Component {
    constructor() {
        super();
        // this._bootstrapAsync();
    }
    _bootstrapAsync = async () => {
        var keys = await AsyncStorage.getAllKeys();
        var token = await AsyncStorage.getItem('Token')
        console.log(keys);
        this.props.navigation.navigate((token ? 'App' : 'Auth'));
    };
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: "black" }}>
                <StatusBar barStyle="dark-content" backgroundColor="white" hidden />
                <Video source={require('./Assets/spash.mp4')}
                    style={{
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        bottom: 0,
                        right: 0
                    }}
                    rate={1}
                    volume={1}
                    muted={true}
                    resizeMode="cover"
                    repeat={false}
                    key="video1"
                    onEnd={this._bootstrapAsync}
                />
            </View>
        );
    }
}